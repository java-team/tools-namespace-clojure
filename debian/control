Source: tools-namespace-clojure
Section: java
Priority: optional
Maintainer: Debian Clojure Maintainers <team+clojure@tracker.debian.org>
Uploaders:
 Apollon Oikonomopoulos <apoikos@debian.org>,
Build-Depends:
 clojure,
 debhelper-compat (= 13),
 default-jdk-headless,
 javahelper,
 libbultitude-clojure <!nocheck>,
 libjava-classpath-clojure <!nocheck>,
 libtest-check-clojure <!nocheck>,
 libtools-reader-clojure <!nocheck>,
 maven-repo-helper,
Standards-Version: 4.7.0
Homepage: https://github.com/clojure/tools.namespace
Vcs-Git: https://salsa.debian.org/clojure-team/tools-namespace-clojure.git
Vcs-Browser: https://salsa.debian.org/clojure-team/tools-namespace-clojure
Rules-Requires-Root: no

Package: libtools-namespace-clojure
Architecture: all
Depends:
 libclojure-java,
 ${java:Depends},
 ${misc:Depends},
Recommends:
 ${java:Recommends},
Description: tools for managing namespaces in Clojure
 A set of tools for managing namespaces in Clojure. Parse `ns` declarations
 from source files, extract their dependencies, build a graph of namespace
 dependencies within a project, update that graph as files change, and
 reload files in the correct order.
 .
 Note that tools.namespace only manages namespace dependencies within a single
 project and is not a substitute for tools like leiningen or maven.
